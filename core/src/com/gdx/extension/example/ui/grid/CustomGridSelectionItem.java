package com.gdx.extension.example.ui.grid;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.gdx.extension.grid.GridSelectionItem;


public class CustomGridSelectionItem extends GridSelectionItem {

    public CustomGridSelectionItem(Skin skin) {
        super(skin);
    }
}
