package com.gdx.extension.example.ui.tab.container;


import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gdx.extension.slide.SlideShow;
import com.gdx.extension.tab.TabContainer;


public class SlideShowContainer extends TabContainer {


    public SlideShowContainer(Skin skin) {
        this(skin, "default");
    }

    public SlideShowContainer(Skin skin, String styleName) {
        super(skin, styleName);

        final SlideShow _vSlideShow = new SlideShow(true, skin);
        add(_vSlideShow).height(200f).row();
        for (int i = 0; i < 10; i++) {
            TextButton _button = new TextButton("Test", skin);
            _button.setTransform(true);
            _button.setWidth(100f);
            _vSlideShow.addItem(_button);
        }

        final SlideShow _slideShow = new SlideShow(false, skin);
        add(_slideShow).width(200f).spaceTop(25f).row();
        for (int i = 0; i < 10; i++) {
            TextButton _button = new TextButton("Test", skin);
            _button.setTransform(true);
            _button.setWidth(50f);
            _slideShow.addItem(_button);
        }

        TextButton _showButtonsButton = new TextButton("Toogle show buttons", skin);
        _showButtonsButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                _vSlideShow.setShowButtons(!_vSlideShow.getShowButtons());
                _slideShow.setShowButtons(!_slideShow.getShowButtons());
            }

        });
        add(_showButtonsButton).spaceTop(25f);
    }
}
