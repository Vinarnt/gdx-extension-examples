package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.utils.Align;
import com.gdx.extension.ProgressBar;
import com.gdx.extension.tab.TabContainer;


public class ProgressBarContainer extends TabContainer
{

    private ProgressBar progressBarSlider;
    private Slider progressSlider;
    
    private ProgressBar progressBarAuto;
    private Stack progressBarAutoStack;
    private Label progressBarAutoLabel;
    
    public ProgressBarContainer(Skin skin)
    {
	super(skin, "default");
	
	progressBarSlider = new ProgressBar(skin);
	add(progressBarSlider).row();
	
	progressSlider = new Slider(0f, 1f, 1f / 100f, false, skin);
	progressSlider.addListener(new ChangeListener() {
	    
	    @Override
	    public void changed(ChangeEvent event, Actor actor)
	    {
		progressBarSlider.setPercent(progressSlider.getValue());
	    }
	    
	});
	progressSlider.fire(new ChangeEvent());
	add(progressSlider).spaceTop(10f).fillX().row();
	
	progressBarAutoStack = new Stack();
	progressBarAuto = new ProgressBar(skin);
	progressBarAutoLabel = new Label("0%", skin);
	progressBarAutoLabel.setAlignment(Align.center);
	progressBarAutoStack.add(progressBarAuto);
	progressBarAutoStack.add(progressBarAutoLabel);
	add(progressBarAutoStack).spaceTop(25f);
    }
    
    @Override
    public void draw(Batch batch, float parentAlpha)
    {
        super.draw(batch, parentAlpha);
        
        float _percent = progressBarAuto.getPercent();
        if(_percent == 1f)
            _percent = 0f;
        
        progressBarAutoLabel.setText(Math.round(_percent * 100f) + "%");
        progressBarAuto.setPercent(_percent + 0.5f * Gdx.graphics.getDeltaTime());
    }

}
