package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gdx.extension.menu.*;
import com.gdx.extension.tab.TabContainer;

public class MenuContainer extends TabContainer {

    private boolean isCheckboxMenuChecked;

    public MenuContainer(Stage stage, Skin skin) {
        super(skin);

        MenuBar _menuBar = new MenuBar(skin);
        ContextMenu _fileMenu = new ContextMenu();
        _fileMenu.addMenu(new BasicMenuItem("Open ...", skin));

        SubMenuItem _subMenu = new SubMenuItem("Recent ...", skin);
        ContextMenu _recentMenu = new ContextMenu();
        _recentMenu.addMenu(new BasicMenuItem("something.txt", skin));
        _recentMenu.addMenu(new BasicMenuItem("somethingelse.txt", skin));
        _recentMenu.addMenu(new BasicMenuItem("again.txt", skin));
        _subMenu.setContextMenu(_recentMenu, stage);
        _fileMenu.addMenu(_subMenu);

        SubMenuItem _testSub = new SubMenuItem("Test", skin);
        ContextMenu _textContext = new ContextMenu();
        _testSub.setContextMenu(_textContext, stage);
        _textContext.addMenu(new BasicMenuItem("Test", skin));
        _fileMenu.addMenu(_testSub);

        CheckboxMenuItem _chekMenu = new CheckboxMenuItem("Check it", skin) {

            @Override
            public boolean rules() {
                return isCheckboxMenuChecked;
            }

        };
        _chekMenu.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                isCheckboxMenuChecked = !isCheckboxMenuChecked;
            }

        });
        _fileMenu.addMenu(_chekMenu);

        TextButton _fileButton = new TextButton("File", skin);
        _fileButton.setWidth(75f);
        _menuBar.addContextMenu(_fileButton, _fileMenu, stage);

        ContextMenu _editMenu = new ContextMenu();
        _editMenu.addMenu(new BasicMenuItem("Copy", skin));
        _editMenu.addMenu(new BasicMenuItem("Cut", skin));
        _editMenu.addMenu(new BasicMenuItem("Paste", skin));

        TextButton _editButton = new TextButton("Edit", skin);
        _editButton.setWidth(75f);
        _menuBar.addContextMenu(_editButton, _editMenu, stage);

        ContextMenu _helpMenu = new ContextMenu();
        _helpMenu.addMenu(new BasicMenuItem("About", skin));

        TextButton _helpButton = new TextButton("Help", skin);
        _helpButton.setWidth(75f);
        _menuBar.addContextMenu(_helpButton, _helpMenu, stage);

        add(_menuBar).expandX().fillX().minHeight(25f).row();

        top();

        ToolBar _toolBar = new ToolBar(skin);
        _toolBar.add(new ImageButton(skin, "new-file"));
        _toolBar.add(new ImageButton(skin, "open"));
        _toolBar.add(new ImageButton(skin, "save"));
        _toolBar.add(new ImageButton(skin, "back"));
        _toolBar.add(new ImageButton(skin, "next"));
        add(_toolBar).expandX().fillX().left();
    }
}
