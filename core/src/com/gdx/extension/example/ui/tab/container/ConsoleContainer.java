package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap.Values;
import com.gdx.extension.Console;
import com.gdx.extension.tab.TabContainer;

public class ConsoleContainer extends TabContainer {

    private Console console;

    public ConsoleContainer(Skin skin) {
        super(skin);

        console = new Console(30, skin);
        add(console).expand().fill();

        console.registerCommand(new TestCommand());

        console.addEntry("Commands available :");
        Values<Console.Command> _commands = console.getCommands().values();
        for (Console.Command _command : _commands) {
            console.addEntry(_command.toString());
        }
        console.addEntry("------------------------------------------------------");
        console.addEntry("Arrow up and Arrow down for commands history.");
    }

    @Override
    public void setStage(Stage stage) {
        super.setStage(stage);

        if (stage != null) {
            console.setFocus();
        }
    }

    private class TestCommand extends Console.Command {

        TestCommand() {
            super("testCmd");

            addParameter(new Console.Command.Parameter("number", Integer.class, false));
            addParameter(new Parameter("isOptional", Boolean.class, true));
        }

        @Override
        public void execute(Console console, Array<Parameter.Value> args) {
            int _number = (Integer) args.get(0).getValue();
            boolean _isOptional = false; // Default value if the optional parameter is not given
            if (args.size > 1) {
                _isOptional = (Boolean) args.get(1).getValue();
            }

            // Do whatever you want

            console.addEntry("Number : " + _number + ", isOptional : " + _isOptional);
        }
    }
}
