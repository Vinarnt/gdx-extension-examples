package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.StringBuilder;
import com.gdx.extension.example.input.InputAction;
import com.gdx.extension.input.ControlBindList;
import com.gdx.extension.input.InputArray;
import com.gdx.extension.input.InputHolder;
import com.gdx.extension.input.InputType;
import com.gdx.extension.tab.TabContainer;

import java.util.Calendar;

public class InputContainer extends TabContainer {

    private ControlBindList bindList;
    private Label timerLabel;

    private InputListener listener;

    private StringBuilder timeBuilder;
    private Calendar calendar;
    private boolean isActive;

    private InputArray comboInputs;
    private Pool<InputHolder> inputHolderPool;
    private ObjectMap<String, InputAction> controlMapping;

    public InputContainer(Skin skin) {
        super(skin);

        comboInputs = new InputArray();
        inputHolderPool = Pools.get(InputHolder.class);
        controlMapping = new ObjectMap<>();

        bindList = new ControlBindList("Escape to cancel", skin);
        add(bindList).row();

        timeBuilder = new StringBuilder();
        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(0L);
        timerLabel = new Label(getTime(), skin);
        add(timerLabel);

        bindList.addInputCatchRow("timerStart", "Start timer",
                new InputArray(
                        new InputHolder(InputType.Keyboard, Keys.CONTROL_LEFT),
                        new InputHolder(InputType.Keyboard, Keys.F1)
                )
        );
        bindList.addInputCatchRow("timerStop", "Stop timer",
                new InputArray(new InputHolder(InputType.Keyboard, Keys.CONTROL_LEFT),
                        new InputHolder(InputType.Keyboard, Keys.F2)
                )
        );

        controlMapping.put("timerStart",
                new InputAction(
                        () -> isActive = true,
                        null));
        controlMapping.put("timerStop",
                new InputAction(
                        () -> isActive = false,
                        null));

        listener = new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                InputHolder _inputHolder = inputHolderPool.obtain();
                _inputHolder.setType(InputType.Mouse);
                _inputHolder.setInput(button);

                comboInputs.add(_inputHolder);

                String _controlTag = bindList.getTag(comboInputs);
                if (_controlTag != null) {
                    InputAction _action = controlMapping.get(_controlTag);
                    if (_action != null) {
                        _action.runOnDownAction();
                    }
                }

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                InputHolder _inputHolder = inputHolderPool.obtain();
                _inputHolder.setType(InputType.Mouse);
                _inputHolder.setInput(button);

                String _controlTag = bindList.getTag(comboInputs);
                if (_controlTag != null) {
                    InputAction _action = controlMapping.get(_controlTag);
                    if (_action != null) {
                        _action.runOnUpAction();
                    }
                }
                comboInputs.removeValue(_inputHolder, false);
                inputHolderPool.free(_inputHolder);
            }

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                InputHolder _inputHolder = inputHolderPool.obtain();
                _inputHolder.setType(InputType.Keyboard);
                _inputHolder.setInput(keycode);

                comboInputs.add(_inputHolder);

                String _controlTag = bindList.getTag(comboInputs);
                if (_controlTag != null) {
                    InputAction _action = controlMapping.get(_controlTag);
                    if (_action != null) {
                        _action.runOnDownAction();
                    }
                }

                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                InputHolder _inputHolder = inputHolderPool.obtain();
                _inputHolder.setType(InputType.Keyboard);
                _inputHolder.setInput(keycode);

                String _controlTag = bindList.getTag(comboInputs);
                if (_controlTag != null) {
                    InputAction _action = controlMapping.get(_controlTag);
                    if (_action != null) {
                        _action.runOnUpAction();
                    }
                }
                comboInputs.removeValue(_inputHolder, false);
                inputHolderPool.free(_inputHolder);

                return true;
            }
        };
    }

    @Override
    protected void setStage(Stage stage) {
        if (stage == null && getStage() != null) {
            getStage().removeCaptureListener(listener);
        } else if (stage != null) {
            stage.addCaptureListener(listener);
        }

        super.setStage(stage);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (isActive) {
            calendar.add(Calendar.MILLISECOND, (int) (delta * 1000));
            timerLabel.setText(getTime());
        }
    }

    private String getTime() {
        timeBuilder.setLength(0);

        timeBuilder
                .append(calendar.get(Calendar.HOUR_OF_DAY))
                .append(':')
                .append(calendar.get(Calendar.MINUTE))
                .append(':')
                .append(calendar.get(Calendar.SECOND))
                .append(':')
                .append(calendar.get(Calendar.MILLISECOND));

        return timeBuilder.toString();
    }
}
