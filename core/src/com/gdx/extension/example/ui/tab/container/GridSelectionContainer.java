package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.gdx.extension.example.ui.grid.CustomGridSelectionItem;
import com.gdx.extension.example.util.Utils;
import com.gdx.extension.grid.GridSelection;
import com.gdx.extension.tab.TabContainer;


public class GridSelectionContainer extends TabContainer {

    private Skin skin;

    private GridSelection<CustomGridSelectionItem> verticalCustomGrid;
    private ScrollPane verticalCustomGridScroll;

    private TextButton deleteButton;

    public GridSelectionContainer(Skin skin) {
        super(skin);

        this.skin = skin;

        verticalCustomGrid = new GridSelection<>(true, 4);

        verticalCustomGridScroll = new ScrollPane(verticalCustomGrid, skin);
        verticalCustomGridScroll.setFadeScrollBars(false);
        if (!Utils.isEmbeded())
            verticalCustomGridScroll.setFlickScroll(false);
        add(verticalCustomGridScroll).height(400f).width(450f);
        verticalCustomGrid.center();
        deleteButton = new TextButton("Remove", skin);
        deleteButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (verticalCustomGrid.getSelection().size > 0)
                    verticalCustomGrid.removeItem(verticalCustomGrid.getSelection().first());
            }

        });
        add(deleteButton).top();

        generate();
    }

    private void generate() {
        for (int i = 0; i < 20; i++) {
            verticalCustomGrid.addItem(new CustomGridSelectionItem(skin));
        }
    }
}
