package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.gdx.extension.example.ui.list.CustomListRow;
import com.gdx.extension.example.util.Utils;
import com.gdx.extension.list.AdvancedList;
import com.gdx.extension.tab.TabContainer;

public class AdvancedListContainer extends TabContainer {

    private Skin skin;

    private ScrollPane customListScroll;
    private AdvancedList<CustomListRow> customList;

    private TextButton deleteButton;

    public AdvancedListContainer(Skin skin) {
        super(skin);

        this.skin = skin;

        customList = new AdvancedList<CustomListRow>();

        customListScroll = new ScrollPane(customList, skin);
        customListScroll.setFadeScrollBars(false);
        customListScroll.setScrollbarsOnTop(true);
        if (!Utils.isEmbeded())
            customListScroll.setFlickScroll(false);
        add(customListScroll).height(300f).minWidth(400f);

        deleteButton = new TextButton("Remove", skin);
        deleteButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Array<CustomListRow> _selection = customList.getSelection();
                if (_selection.size > 0)
                    customList.removeItem(_selection.first());
            }

        });
        add(deleteButton).top();

        generate();
    }

    private void generate() {
        for (float i = 0; i < 256f; i += 10f) {
            Pixmap _pixMap = new Pixmap(32, 32, Format.RGBA8888);
            _pixMap.setColor(i / 256f, i / 256f, i / 256f, 1f);
            _pixMap.fill();
            customList.addItem(new CustomListRow(_pixMap, i + ";" + i + ";" + i, skin));
        }
    }

}
