package com.gdx.extension.example.util;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;


public class Utils
{

    public static boolean isEmbeded()
    {
	return Gdx.app.getType() == ApplicationType.Android || Gdx.app.getType() == ApplicationType.iOS;
    }
    
}
