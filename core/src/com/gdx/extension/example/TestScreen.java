package com.gdx.extension.example;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.gdx.extension.BaseScreen;
import com.gdx.extension.ScreenManager;
import com.gdx.extension.example.ui.tab.container.*;
import com.gdx.extension.tab.Tab;
import com.gdx.extension.tab.TabPane;

public class TestScreen extends BaseScreen {

    private TabPane tabPane;

    TestScreen(ScreenManager screenManager) {
        super(screenManager, 1);

        tabPane = new TabPane(skin);
        tabPane.addTab(new Tab(new Label("ProgressBar", skin), new ProgressBarContainer(skin), skin));
        tabPane.addTab(new Tab(new Label("AdvancedList", skin), new AdvancedListContainer(skin),
                skin));
        tabPane.addTab(new Tab(new Label("GridSelection", skin), new GridSelectionContainer(skin),
                skin));
        tabPane.addTab(new Tab(new Label("SlideShow", skin), new SlideShowContainer(skin), skin));
        tabPane.addTab(new Tab(new Label("Menus", skin), new MenuContainer(
                screenManager.getStage(), skin), skin));
        tabPane.addTab(new Tab(new Label("Colors", skin), new ColorContainer(skin), skin));
        tabPane.addTab(new Tab(new Label("Console", skin), new ConsoleContainer(skin), skin));
        tabPane.addTab(new Tab(new Label("Inputs", skin), new InputContainer(skin), skin));
        layout.add(tabPane).expand().fill();
    }
}