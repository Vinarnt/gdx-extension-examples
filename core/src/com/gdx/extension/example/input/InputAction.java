package com.gdx.extension.example.input;

public class InputAction {

    private Runnable onDownAction;
    private Runnable onUpAction;

    public InputAction(Runnable onDownAction, Runnable onUpAction) {
	this.onDownAction = onDownAction;
	this.onUpAction = onUpAction;
    }

    public void runOnDownAction() {
	if (onDownAction != null) {
	    onDownAction.run();
	}
    }

    public void runOnUpAction() {
	if (onUpAction != null) {
	    onUpAction.run();
	}
    }

}
