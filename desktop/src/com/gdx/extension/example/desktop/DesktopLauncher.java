package com.gdx.extension.example.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gdx.extension.example.ApplicationCore;

public class DesktopLauncher {

    public static void main(String[] arg) {
	LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
	cfg.title = "LibGdx-extension - Examples";
	cfg.width = 1024;
	cfg.height = 720;
	cfg.vSyncEnabled = true;
	cfg.foregroundFPS = 0;

	new LwjglApplication(ApplicationCore.getInstance(), cfg);
    }
}
